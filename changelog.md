# Changelog

## [3.2] - 2021-12-13
## Add
- Check transfered recording sizes

## [1.0.1] - 2020-08-21
### Add
- This changelog file
- SCW library as a Class
